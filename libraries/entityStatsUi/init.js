/**
 * @file
 * Entity stats UI.
 */

(function ($, Drupal, document) {

  Drupal.behaviors.entityStatsUi = {
    attach: function () {

      // Make tables sortable with JS.
      $('.sortable').tablesorter();

    }
  };

})(jQuery, Drupal, document);

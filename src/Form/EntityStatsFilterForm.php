<?php

namespace Drupal\entity_stats\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\entity_stats\EntityStatsManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the form for filter Students.
 */
class EntityStatsFilterForm extends FormBase {

  /**
   * The EntityStatsManager
   *
   * @var \Drupal\entity_stats\EntityStatsManager
   */
  protected $entityStatsManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityStatsManager $entityStatsManager) {
    $this->entityStatsManager = $entityStatsManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_stats.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'entity_stats_filter_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['filters'] = [
      '#type' => 'container',
    ];

    $options = $this->entityStatsManager->getContentEntityTypesOptions();

    $form['filters']['entityTypes'] = [
      '#type' => 'select',
      '#title' => 'Content Entity types',
      '#options' => array_combine($options, $options),
      '#size' => 6,
      '#multiple' => TRUE,
    ];

    $form['filters']['actions'] = [
      '#type' => 'actions',
    ];

    $form['filters']['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => 'Filter',
    ];

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (empty($form_state->getValue('entityTypes'))) {
      $form_state->setErrorByName('entityTypes', $this->t('Please choose an option.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $url = Url::fromRoute('entity_stats.content_entities');
    $url->setOption('query', ['entityTypes' => $form_state->getValue('entityTypes')]);
    $form_state->setRedirectUrl($url);
  }

}

<?php

namespace Drupal\entity_stats\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\entity_stats\EntityStatsManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Main entity statistics controller.
 */
class EntityStatsTextFormatsController extends ControllerBase {

  /**
   * The EntityStatsManager
   *
   * @var \Drupal\entity_stats\EntityStatsManager
   */
  protected $entityStatsManager;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityStatsManager $entityStatsManager, RendererInterface $renderer, ModuleHandlerInterface $moduleHandler) {
    $this->entityStatsManager = $entityStatsManager;
    $this->renderer = $renderer;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_stats.manager'),
      $container->get('renderer'),
      $container->get('module_handler')
    );
  }

  /**
   * Text formats statistic page.
   */
  public function stats() {

    $filterFormatsEntities = $this->entityStatsManager->getFilterFormats();
    $filterFormatsAll = array_keys($filterFormatsEntities);

    $header = [
      'ID',
      'Name',
      'Set on N Fields',
      'Actual N Fields using it',
      'By field usage actual',
    ];

    $meta = [];
    $rows = [];

    $allFields = $this->entityStatsManager->getFieldsDataOfSupportingFilterFormat();

    // Gather all needed data.
    foreach ($allFields as $fieldDataPerEntityType) {
      foreach ($fieldDataPerEntityType as $entityTypeId => $fieldsData) {
        foreach ($fieldsData as $fieldName => $fieldData) {
          foreach ($fieldData['bundles'] as $bundle) {
            $fieldsDefinition = $this->entityStatsManager->getFieldsDefinition($entityTypeId, $bundle);

            if (!$fieldsDefinition[$fieldName] instanceof FieldDefinitionInterface || !method_exists($fieldsDefinition[$fieldName], 'getThirdPartySettings')) {
              // Taxonomy description field case usually.
              continue;
            }

            $filterTypes = $filterFormatsAll;

            // Support better_formats module offering refined list of
            // Text Filter per Field instance.
            if ($this->moduleHandler->moduleExists('better_formats')) {
              $betterFormatsConfig = $fieldsDefinition[$fieldName]->getThirdPartySettings('better_formats');
              if (!empty($betterFormatsConfig['allowed_formats_toggle'])) {
                $filterTypes = array_filter($betterFormatsConfig['allowed_formats']);
              }
            }

            // Gather usage count straight from DB.
            foreach ($filterTypes as $filterType) {
              $query = \Drupal::database()
                ->select("{$entityTypeId}__{$fieldName}", 'f')
                ->condition("f.{$fieldName}_format", $filterType)
                ->condition('f.bundle', $bundle)
                ->condition('f.deleted', 0);

              $meta[$filterType][$entityTypeId][$bundle][$fieldName] = $query->countQuery()
                ->execute()
                ->fetchField();
            }
          }
        }
      }
    }

    $tempMeta = [];
    foreach ($meta as $filterType => $byType) {
      $tempMeta[$filterType] ??= [];
      $tempMeta[$filterType]['count_n_fields'] ??= 0;
      $tempMeta[$filterType]['field_usage_cnt'] ??= [];
      $tempMeta[$filterType]['field_usage_cnt_total'] = 0;

      foreach ($byType as $entityTypeId => $byBundle) {
        $tempMeta[$filterType]['field_usage'][$entityTypeId] ??= [];
        $tempMeta[$filterType]['field_usage_cnt'][$entityTypeId] ??= [];

        foreach ($byBundle as $bundle => $byFieldName) {
          foreach ($byFieldName as $fieldName => $usageCnt) {

            $tempMeta[$filterType]['field_usage'][$entityTypeId]["{$bundle}/$fieldName"] ??= 0;
            $tempMeta[$filterType]['field_usage_cnt'][$entityTypeId]["{$bundle}/$fieldName"] ??= 0;

            $rows[$filterType] = [
              0 => $filterType,
              1 => $filterFormatsEntities[$filterType]->label(),
            ];

            // Gather data.
            $tempMeta[$filterType]['count_n_fields']++;
            $tempMeta[$filterType]['field_usage_cnt'][$entityTypeId]["{$bundle}/$fieldName"] += $usageCnt;
            $tempMeta[$filterType]['field_usage_cnt_total'] += $usageCnt;
          }
        }
      }

      $rows[$filterType][2] = $tempMeta[$filterType]['count_n_fields'];
      $rows[$filterType][3] = $tempMeta[$filterType]['field_usage_cnt_total'];

      $renderableData = [];
      foreach ($tempMeta[$filterType]['field_usage_cnt'] as $entityTypeId => $fieldUsage) {

        $fieldHtml = [];
        foreach ($fieldUsage as $key => $fieldCnt) {
          $fieldHtml[] = "{$key}({$fieldCnt})";
        }

        $renderableData[] = [
          '#type' => 'inline_template',
          '#template' => '<strong>{{ entityTypeId }}:</strong><p style="max-width:200px;">{{fieldNames}}</p>',
          '#context' => [
            'entityTypeId' => $entityTypeId,
            'fieldNames' => implode(', ', $fieldHtml),
          ],
        ];
      }

      $rows[$filterType][4] = $this->renderer->render($renderableData);
      ksort($rows[$filterType]);
    }

    $build['table'] = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#attributes' => ['class' => ['sortable sticky-enabled']],
      '#attached' => ['library' => ['entity_stats/init']],
    ];

    return $build;
  }

}

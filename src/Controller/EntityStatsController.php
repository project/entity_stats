<?php

namespace Drupal\entity_stats\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityFormBuilder;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Url;
use Drupal\entity_stats\EntityStatsManager;
use Drupal\entity_stats\Form\EntityStatsFilterForm;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Entity statistics controller.
 */
class EntityStatsController extends ControllerBase {

  /**
   * Form Builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface|EntityFormBuilder
   */
  protected $formBuilder;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The EntityStatsManager
   *
   * @var \Drupal\entity_stats\EntityStatsManager
   */
  protected $entityStatsManager;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public function __construct(FormBuilderInterface $formBuilder, RequestStack $requestStack, EntityStatsManager $entityStatsManager, RendererInterface $renderer) {
    $this->formBuilder = $formBuilder;
    $this->requestStack = $requestStack;
    $this->entityStatsManager = $entityStatsManager;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('form_builder'),
      $container->get('request_stack'),
      $container->get('entity_stats.manager'),
      $container->get('renderer')
    );
  }

  /**
   * Main statistic page.
   */
  public function stats($entityTypeId) {
    $build = $entityTypeId === 'all' ?
      $this->buildStatsAll() : $this->buildStatsPerEntityType($entityTypeId);

    $build['#attached']['library'][] = 'entity_stats/init';
    return $build;
  }

  private function buildStatsAll(): array {
    $build['filters'] = $this->formBuilder->getForm(EntityStatsFilterForm::class);

    $contentEntityTypes = $this->requestStack->getCurrentRequest()->query->all('entityTypes')
      ?: $this->entityStatsManager->getContentEntityTypesOptions();

    $header = [
      'ID',
      'Bundle count',
      'N Entities',
      'Published',
      'Unpublished',
      'Revisions',
      '~Fields',
      'Referencing fields',
      '--- More ---',
    ];

    $rows = [];
    foreach ($contentEntityTypes ?? [] as $contentEntityTypeId) {
      $bundleCount = $this->entityStatsManager->getContentEntityBundleCount($contentEntityTypeId);
      $entitiesCount = $this->entityStatsManager->getContentEntityContentCount($contentEntityTypeId);
      $referencingFields = $this->entityStatsManager->getEntityReferencingFields($contentEntityTypeId);

      $rows[$contentEntityTypeId] = [
        $contentEntityTypeId,
        $bundleCount,
        $entitiesCount['total'],
        $entitiesCount['published'],
        $entitiesCount['unpublished'],
        $this->entityStatsManager->getContentRevisionCount($contentEntityTypeId),
        $this->entityStatsManager->getContentEntityFieldsCount($contentEntityTypeId),
        $this->renderer->render($referencingFields) ?: '---',
      ];

      $moreLinks = $this->renderer->executeInRenderContext(new RenderContext(), function () use ($contentEntityTypeId, $bundleCount) {
        $moreLinksBuild = [
          '#type' => 'dropbutton',
          '#links' => [
            'field_stats' => [
              'title' => 'Field stats',
              'url' => Url::fromRoute('entity_stats.fields_per_type', ['entityTypeId' => $contentEntityTypeId]),
            ],
          ],
        ];

        if ($bundleCount > 1) {
          $moreLinksBuild['#links']['per_bundle'] = [
            'title' => "Per bundle",
            'url' => Url::fromRoute('entity_stats.content_entities', ['entityTypeId' => $contentEntityTypeId]),
          ];
        }

        return $this->renderer->render($moreLinksBuild);
      });
      $rows[$contentEntityTypeId][] = $moreLinks;
    }

    $build['table'] = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#attributes' => ['class' => ['sortable sticky-enabled']],
    ];

    return $build;
  }

  private function buildStatsPerEntityType($contentEntityTypeId): array {

    $header = [
      'ID',
      'N Entities',
      'Published',
      'Unpublished',
      'Revisions',
      '~Fields',
      'Referencing fields',
      '--- More ---',
    ];

    $rows = [];
    foreach (array_keys($this->entityStatsManager->getContentEntityBundles($contentEntityTypeId)) as $contentEntityBundleId) {
      $entitiesCount = $this->entityStatsManager->getContentEntityContentCount($contentEntityTypeId, $contentEntityBundleId);
      $referencingFields = $this->entityStatsManager->getEntityReferencingFields($contentEntityTypeId, $contentEntityBundleId);

      $rows[$contentEntityBundleId] = [
        $contentEntityBundleId,
        $entitiesCount['total'],
        $entitiesCount['published'],
        $entitiesCount['unpublished'],
        $this->entityStatsManager->getContentRevisionCount($contentEntityTypeId, $contentEntityBundleId),
        $this->entityStatsManager->getContentEntityFieldsCount($contentEntityTypeId, $contentEntityBundleId),
        $this->renderer->render($referencingFields) ?: '---',
      ];

      $moreLinks = $this->renderer->executeInRenderContext(new RenderContext(), function () use ($contentEntityTypeId, $contentEntityBundleId) {
        $moreLinksBuild = [
          '#type' => 'dropbutton',
          '#links' => [
            'per_entity_type' => [
              'title' => 'Field stats',
              'url' => Url::fromRoute('entity_stats.fields_per_type', [
                'entityTypeId' => $contentEntityTypeId,
                'bundle' => $contentEntityBundleId,
              ]),
            ],
          ],
        ];
        return $this->renderer->render($moreLinksBuild);
      });

      $rows[$contentEntityBundleId][] = $moreLinks;
    }

    $build['table'] = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#attributes' => ['class' => ['sortable sticky-enabled']],
    ];

    return $build;
  }

  public function getTitle($entityTypeId): string {
    return $entityTypeId === 'all' ?
      "Entity Statistics" : "Entity Statistics for {$entityTypeId}";
  }

}

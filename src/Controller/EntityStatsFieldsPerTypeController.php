<?php

namespace Drupal\entity_stats\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityFormBuilder;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Url;
use Drupal\entity_stats\EntityStatsManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Entity statistics per type controller.
 */
class EntityStatsFieldsPerTypeController extends ControllerBase {

  /**
   * Form Builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface|EntityFormBuilder
   */
  protected $formBuilder;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The EntityStatsManager
   *
   * @var \Drupal\entity_stats\EntityStatsManager
   */
  protected $entityStatsManager;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public function __construct(FormBuilderInterface $formBuilder, RequestStack $requestStack, EntityStatsManager $entityStatsManager, RendererInterface $renderer) {
    $this->formBuilder = $formBuilder;
    $this->requestStack = $requestStack;
    $this->entityStatsManager = $entityStatsManager;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('form_builder'),
      $container->get('request_stack'),
      $container->get('entity_stats.manager'),
      $container->get('renderer')
    );
  }

  /**
   * Entity statistics per type page.
   */
  public function stats(string $entityTypeId, string $bundle): array {
    $bundle = ($bundle === 'all') ? NULL : $bundle;

    $build['legend'] = [
      '#type' => 'details',
      '#group' => 'advanced',
      '#title' => 'Legend',
      '#open' => FALSE,
      '#weight' => -10,
      'values' => [
        '#theme' => 'item_list',
        '#list_type' => 'ul',
        '#items' => [
          ['#markup' => "N - count"],
          ['#markup' => "NaN/NA - not a value in current context"],
          ['#markup' => "The table is sortable"],
          ['#markup' => "'Referencing fields' reads as: ENTITY_TYPE: BUNDLE/FIELD_NAME(N_CONTENT_REFERENCES_TO_THIS_COMBINATION)"],
          ['#markup' => "Some Entity Types(block_content, taxonomy_term, paragraph,...) aren't implementing properly the Revision API (revision_created meta) meaning we can't retrieve change count for a specific period, only total change count"],
          ['#markup' => "Base fields defined on entities are excluded"],
          ['#markup' => "Fields configured to have a default values(except 'boolean') - might expose 'Emptiness' stat not accurate"],
          ['#markup' => "Stats are based on the default language content while Updated count is based on all revisions for all languages"],
          [
            '#type' => 'inline_template',
            '#template' => 'See <a target="_blank" href="{{url}}">JSON version</a>',
            '#context' => [
              'url' => Url::fromRoute('entity_stats.json')->toString(),
            ],
          ],
        ],
      ],
    ];

    $header = [
      'field_name',
      'Field label',
      'Field type',
      'Updated N(2w)',
      'Updated N(1m)',
      'Updated N(3m)',
      'Updated N(All time)',
      'Emptiness (%/Total/Empty)',
      'Bundles(N)',
      'Per bundle stats',
    ];

    $rows = [];
    foreach ($this->entityStatsManager->getContentEntityFields($entityTypeId, $bundle) as $fieldName => $fieldData) {
      $fieldPopulationCnt = $this->entityStatsManager->getPopulationFieldValCount($entityTypeId, $fieldName, $bundle);
      // If we have 1 bundle in the context let's retrieve the field label.
      $bundleForFieldLabel = $bundle ??
        count($fieldData['bundles']) == 1 ? reset($fieldData['bundles']) : NULL;

      $rows[$fieldName] = [
        $fieldName,
        $this->entityStatsManager->getFieldLabel($entityTypeId, $bundleForFieldLabel, $fieldName),
        $this->entityStatsManager->getFieldType($entityTypeId, $bundleForFieldLabel, $fieldName),
        $this->entityStatsManager->getContentRevisionCountPerField($entityTypeId, $fieldName, '-2 week', $bundle),
        $this->entityStatsManager->getContentRevisionCountPerField($entityTypeId, $fieldName, '-1 month', $bundle),
        $this->entityStatsManager->getContentRevisionCountPerField($entityTypeId, $fieldName, '-3 month', $bundle),
        $this->entityStatsManager->getContentRevisionCountPerField($entityTypeId, $fieldName, NULL, $bundle),
        "{$fieldPopulationCnt['percentage']} / {$fieldPopulationCnt['total']} / {$fieldPopulationCnt['empty']}",
        count($fieldData['bundles']),
      ];

      // Actions.
      $rows[$fieldName][] = $this->renderer->executeInRenderContext(new RenderContext(), function () use ($entityTypeId, $fieldData, $bundle) {
        // Link to the per bundle field stats page.
        $moreLinksBuild = [
          '#type' => 'dropbutton',
        ];

        foreach ($fieldData['bundles'] as $thisBundle) {
          // Prevent linking to itself.
          if ($thisBundle === $bundle) {
            continue;
          }
          $moreLinksBuild['#links']['per_bundle_' . $thisBundle] = [
            'title' => $thisBundle,
            'url' => Url::fromRoute('entity_stats.fields_per_type', [
              'entityTypeId' => $entityTypeId,
              'bundle' => $thisBundle,
            ]),
          ];
        }
        return $this->renderer->render($moreLinksBuild);
      });;
    }

    $build['table'] = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#attributes' => ['class' => ['sortable sticky-enabled']],
    ];

    $build['#attached']['library'][] = 'entity_stats/init';
    return $build;
  }

  public function getTitle($entityTypeId, $bundle): string {
    return $bundle === 'all' ?
      "Entity Field Statistics for {$entityTypeId}" : "Entity Field Statistics for {$entityTypeId}/{$bundle}";
  }

}

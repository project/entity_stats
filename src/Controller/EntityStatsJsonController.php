<?php

namespace Drupal\entity_stats\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityFormBuilder;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\entity_stats\EntityStatsManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Main entity statistics controller.
 */
class EntityStatsJsonController extends ControllerBase {

  /**
   * Form Builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface|EntityFormBuilder
   */
  protected $formBuilder;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The EntityStatsManager
   *
   * @var \Drupal\entity_stats\EntityStatsManager
   */
  protected $entityStatsManager;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public function __construct(FormBuilderInterface $formBuilder, RequestStack $requestStack, EntityStatsManager $entityStatsManager, RendererInterface $renderer) {
    $this->formBuilder = $formBuilder;
    $this->requestStack = $requestStack;
    $this->entityStatsManager = $entityStatsManager;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('form_builder'),
      $container->get('request_stack'),
      $container->get('entity_stats.manager'),
      $container->get('renderer')
    );
  }

  /**
   * All statistic as json.
   */
  public function stats() {
    $data = [];

    $contentEntityTypes = $this->entityStatsManager->getContentEntityTypesOptions();
    foreach ($contentEntityTypes as $entityTypeId) {
      foreach (array_keys($this->entityStatsManager->getContentEntityBundles($entityTypeId)) as $bundle) {
        $data[$entityTypeId][$bundle]['entities_cnt'] = $this->entityStatsManager->getContentEntityContentCount($entityTypeId, $bundle);
        $data[$entityTypeId][$bundle]['total_revisions_cnt'] = $this->entityStatsManager->getContentRevisionCount($entityTypeId, $bundle);
        $data[$entityTypeId][$bundle]['total_fields_cnt'] = $this->entityStatsManager->getContentEntityFieldsCount($entityTypeId, $bundle);
        $data[$entityTypeId][$bundle]['fields_stats'] = $this->getFieldsStats($entityTypeId, $bundle);
        $data[$entityTypeId][$bundle]['referencing_fields'] = $this->entityStatsManager->getEntityReferenceFieldsData([
          'entityTypeId' => $entityTypeId,
          'bundle' => $bundle,
        ]);
      }
    }

    $response = new JsonResponse();
    return $response->setData($data);
  }

  /**
   * Based on EntityStatsFieldsPerTypeController.
   *
   * @param string $entityTypeId
   * @param null $bundle
   *
   * @return array
   */
  private function getFieldsStats(string $entityTypeId, $bundle = NULL): array {

    $data = [];

    foreach ($this->entityStatsManager->getContentEntityFields($entityTypeId, $bundle) as $fieldName => $fieldData) {
      $fieldPopulationCnt = $this->entityStatsManager->getPopulationFieldValCount($entityTypeId, $fieldName, $bundle);
      // If we have 1 bundle in the context let's retrieve the field label.
      $bundleForFieldLabel = $bundle ??
        count($fieldData['bundles']) == 1 ? reset($fieldData['bundles']) : NULL;

      $data[$fieldName] = [
        'label' => $this->entityStatsManager->getFieldLabel($entityTypeId, $bundleForFieldLabel, $fieldName) ?: NULL,
        'type' => $this->entityStatsManager->getFieldType($entityTypeId, $bundleForFieldLabel, $fieldName) ?: NULL,
        'used_in_bundles' => $fieldData['bundles'] ?: NULL,
        'changed_cnt_last_2w' => $this->entityStatsManager->getContentRevisionCountPerField($entityTypeId, $fieldName, '-2 week', $bundle),
        'changed_cnt_last_1m' => $this->entityStatsManager->getContentRevisionCountPerField($entityTypeId, $fieldName, '-1 month', $bundle),
        'changed_cnt_last_3m' => $this->entityStatsManager->getContentRevisionCountPerField($entityTypeId, $fieldName, '-3 month', $bundle),
        'changed_cnt_total' => $this->entityStatsManager->getContentRevisionCountPerField($entityTypeId, $fieldName, NULL, $bundle),
        'emptiness' => $fieldPopulationCnt,
      ];
    }

    return $data;
  }

}

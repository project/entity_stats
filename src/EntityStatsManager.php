<?php

namespace Drupal\entity_stats;

use Drupal;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityTypeRepositoryInterface;
use Drupal\Core\Entity\ContentEntityType;
use Drupal\Core\Entity\EditorialContentEntityBase;
use Drupal\Core\Render\RendererInterface;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Core\Entity\RevisionLogInterface;

/**
 * The entity_stats manager class.
 */
class EntityStatsManager {

  /**
   * No number available identification.
   */
  const NO_NUM_VAL = 'NaN';

  /**
   * No value identification.
   */
  const NO_VAL = 'NA';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The entity type repository service.
   *
   * @var \Drupal\Core\Entity\EntityTypeRepositoryInterface
   */
  protected $entityTypeRepository;

  /**
   * Entity type bundle info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * Entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The entity repository.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, EntityTypeRepositoryInterface $entityTypeRepository, EntityTypeBundleInfoInterface $entityTypeBundleInfo, EntityFieldManagerInterface $entityFieldManager, EntityRepositoryInterface $entityRepository, RendererInterface $renderer) {
    $this->entityTypeManager = $entityTypeManager;
    $this->entityTypeRepository = $entityTypeRepository;
    $this->entityTypeBundleInfo = $entityTypeBundleInfo;
    $this->entityFieldManager = $entityFieldManager;
    $this->entityRepository = $entityRepository;
    $this->renderer = $renderer;
  }

  public function getEntityReferencingFields(string $entityTypeId, $bundle = NULL): array {

    $fieldsData = $this->getEntityReferenceFieldsData([
      'entityTypeId' => $entityTypeId,
      'bundle' => $bundle,
    ]);

    $build = [];
    foreach ($fieldsData ?? [] as $eTypeName => $thisFieldsData) {

      $filterFields = array_map(function ($thisFieldData) use ($bundle) {
        if ($bundle && in_array($bundle, $thisFieldData['target_bundles'], TRUE)) {
          return "{$thisFieldData['in_bundle']}/{$thisFieldData['name']}({$thisFieldData['values_cnt']})";
        }
        elseif (!$bundle && !empty($thisFieldData['in_bundle'])) {
          return "{$thisFieldData['in_bundle']}/{$thisFieldData['name']}({$thisFieldData['values_cnt']})";
        }
        elseif (!$bundle) {
          return "{$thisFieldData['name']}";
        }
      }, $thisFieldsData);

      $filterFields = array_filter($filterFields);
      sort($filterFields);
      if (empty($filterFields)) {
        continue;
      }

      // @idea: Find a better way to show all this info.
      $build[] = [
        '#type' => 'inline_template',
        '#template' => '<strong>{{ entityTypeId }}:</strong><p style="max-width:200px;">{{fieldNames}}</p>',
        '#context' => [
          'entityTypeId' => $eTypeName,
          'fieldNames' => implode(', ', $filterFields),
        ],
      ];
    }

    return $build;
  }

  public function getEntityReferenceFieldsData(array $filter = [
    'entityTypeId' => NULL,
    'bundle' => NULL,
  ]): array {

    $fieldsData = [];
    $entityTypes = $this->entityTypeManager->getDefinitions();
    foreach ($entityTypes as $thisEntityTypeId => $thisEntityType) {
      if (!($thisEntityType instanceof ContentEntityType)) {
        continue;
      }

      $properties = $this->entityFieldManager->getBaseFieldDefinitions($thisEntityTypeId);
      $bundles = $this->entityTypeBundleInfo->getBundleInfo($thisEntityTypeId);
      foreach ($bundles as $bundle => $info) {
        $properties += $this->entityFieldManager->getFieldDefinitions($thisEntityTypeId, $bundle);
      }

      foreach ($properties as $name => $property) {
        if (!in_array($property->getType(), [
          'entity_reference',
          'entity_reference_revisions',
        ])) {
          continue;
        }

        $settings = $property->getSettings();
        $targetBundles = $settings['handler_settings']['target_bundles'] ?? [];
        $attachedToEntityType = $settings['target_type'];
        $attachedToBundle = $property->getTargetBundle();
        if (empty($attachedToEntityType) || empty($targetBundles)) {
          continue;
        }

        if (!empty($filter['bundle']) && !in_array($filter['bundle'], $targetBundles)) {
          continue;
        }

        $fieldsData[$attachedToEntityType][$thisEntityTypeId][] = [
          'in_bundle' => $attachedToBundle,
          'target_bundles' => $targetBundles,
          'name' => $name,
          'values_cnt' => $this->getPopulationFieldValCount($thisEntityTypeId, $name, $attachedToBundle)['exists'] ?? self::NO_NUM_VAL,
        ];
      }
    }

    if (!empty($filter['entityTypeId'])) {
      return $fieldsData[$filter['entityTypeId']] ?? [];
    }

    return $fieldsData;
  }

  public function getFieldType(string $entityTypeId, $bundle, $fieldName): string {
    $fieldDefinition = $this->getFieldsDefinition($entityTypeId, $bundle);
    if (isset($fieldDefinition[$fieldName])) {
      return $fieldDefinition[$fieldName]->getType() ?: self::NO_VAL;
    }

    return self::NO_VAL;
  }

  public function getFieldLabel(string $entityTypeId, $bundle, $fieldName): string {
    $fieldDefinition = $this->getFieldsDefinition($entityTypeId, $bundle);
    if (isset($fieldDefinition[$fieldName])) {
      return $fieldDefinition[$fieldName]->getLabel() ?: self::NO_VAL;
    }

    return self::NO_VAL;
  }

  public function getFieldsDefinition(string $entityTypeId, $bundle): array {
    static $fieldsDefinition = [];
    $cid = "{$entityTypeId}__{$bundle}";

    if (!isset($fieldsDefinition[$cid])) {
      $fieldsDefinition[$cid] = $this->entityFieldManager->getFieldDefinitions($entityTypeId, $bundle);
    }

    return $fieldsDefinition[$cid];
  }

  public function getContentEntityTypesOptions(): array {
    return array_keys($this->entityTypeRepository->getEntityTypeLabels(TRUE)['Content']);
  }

  public function getContentEntityContentCount(string $entityTypeId, $bundle = NULL) {
    try {
      if (!$entityType = $this->getContentEntityTypeDefinition($entityTypeId)) {
        return self::NO_NUM_VAL;
      }

      $entities = $this->getEntities($entityTypeId, $bundle);
      $published = $unpublished = 0;

      foreach ($entities as $entity) {
        // Ensure it has published definition.
        if ($entityType->hasKey('published')
          && is_subclass_of($entityType->getClass(), EntityPublishedInterface::class)
        ) {
          if ($entity->isPublished()) {
            $published++;
          }
          else {
            $unpublished++;
          }
        }
        // If not just increase published count.
        else {
          $published++;
        }
      }

      return [
        'total' => count($entities),
        'published' => $published,
        'unpublished' => $unpublished,
      ];

    } catch (\Exception $e) {
      return self::NO_NUM_VAL;
    }
  }

  public function getContentRevisionCount(string $entityTypeId, $bundle = NULL): int|string|null {
    $entityType = $this->getContentEntityTypeDefinition($entityTypeId);
    if (is_subclass_of($entityType->getClass(), EditorialContentEntityBase::class)) {
      $query = $this->entityTypeManager->getStorage($entityTypeId)->getQuery();
      if ($bundle && $entityType->hasKey('bundle')) {
        $query->condition($entityType->getKey('bundle'), $bundle);
      }
      $query->allRevisions()
        ->accessCheck(FALSE);
      return count($query->execute());
    }

    return self::NO_NUM_VAL;
  }

  public function getContentEntityBundleCount(string $entityTypeId): int {
    return $this->getContentEntityTypeDefinition($entityTypeId)
      ? count($this->getContentEntityBundles($entityTypeId)) : 0;
  }

  public function getContentEntityBundles(string $entityTypeId): array {
    return $this->getContentEntityTypeDefinition($entityTypeId)
      ? $this->entityTypeBundleInfo->getBundleInfo($entityTypeId) : [];
  }

  public function getContentEntityFieldsCount(string $entityTypeId, $bundle = NULL): int {
    return count($this->getContentEntityFields($entityTypeId, $bundle));
  }

  public function getContentEntityFields(string $entityTypeId, $bundle = NULL): array {
    $allFields = $this->entityFieldManager->getFieldMap();
    $fields = $allFields[$entityTypeId] ?? [];

    // Limit to required bundle.
    if ($bundle && $this->getContentEntityBundleCount($entityTypeId) > 1) {
      foreach ($fields as $fieldName => $fieldDatum) {
        if (!in_array($bundle, $fieldDatum['bundles'])) {
          unset($fields[$fieldName]);
        }
      }
    }

    // Do not include base entity fields/columns.
    foreach ($this->getContentEntityBaseFieldKeys($entityTypeId) ?? [] as $fieldColumnToSkip) {
      if (isset($fields[$fieldColumnToSkip])) {
        unset($fields[$fieldColumnToSkip]);
      }
    }
    return $fields;
  }

  public function getContentEntityTypeDefinition(string $entityTypeId): ?ContentEntityType {
    $entityTypeDefinition = $this->entityTypeManager->getDefinition($entityTypeId);
    return $entityTypeDefinition instanceof ContentEntityType ?
      $entityTypeDefinition : NULL;
  }

  private function getContentEntityBaseFieldKeys(string $entityTypeId): ?array {
    if ($entityTypeDefinition = $this->getContentEntityTypeDefinition($entityTypeId)) {
      return $entityTypeDefinition->getKeys();
    }
    return NULL;
  }

  public function getPopulationFieldValCount(string $entityTypeId, string $fieldName, $bundle = NULL): array {
    $entities = $this->getEntities($entityTypeId, $bundle);
    $totalValuesCnt = $emptyValuesCnt = 0;
    foreach ($entities as $id => $entity) {
      if (!$entity->hasField($fieldName)) {
        continue;
      }

      $totalValuesCnt++;

      // Detect boolean fields to give it special care.
      try {
        /** @var Drupal\Core\Field\BaseFieldDefinition $fieldsDefinition */
        $fieldsDefinition = $entity->getFieldDefinition($fieldName);
        $isBoolField = $fieldsDefinition ? $fieldsDefinition->getType() === 'boolean' : NULL;
      } catch (\Exception $e) {
        $isBoolField = FALSE;
      }

      // For boolean field type and probably all others defining the default
      // value on field instance level, "isEmpty()" won't give us any meaningful
      // state as there is always a value in it.
      if ($isBoolField) {
        $defaultVal = $fieldsDefinition->getDefaultValue($entity);
        $thisFieldVal = $entity->get($fieldName)->getValue();

        // Compare default field val with current one.
        if (str_replace(['"', 'false', 'true'], ['', '0', '1'], json_encode($defaultVal)) === str_replace([
            '"',
            'false',
            'true',
          ], ['', '0', '1'], json_encode($thisFieldVal))) {
          $emptyValuesCnt++;
        }
      }
      else {
        if ($entity->get($fieldName)->isEmpty()) {
          $emptyValuesCnt++;
        }
      }
    }

    $percentage = $totalValuesCnt ? round($emptyValuesCnt / $totalValuesCnt, 2) * 100 : 0;
    // Adjust the percentage so it makes sense.
    if ($percentage === 0 && $emptyValuesCnt === 0 && $totalValuesCnt > 0) {
      $percentage = 100;
    }

    return [
      'total' => $totalValuesCnt,
      'empty' => $emptyValuesCnt,
      'exists' => $totalValuesCnt - $emptyValuesCnt,
      'percentage' => !is_nan($percentage) ? $percentage : 0,
    ];
  }

  public function getContentRevisionCountPerField(string $entityTypeId, string $fieldName, $period = NULL, $bundle = NULL) {

    $entityType = $this->getContentEntityTypeDefinition($entityTypeId);
    if (!is_subclass_of($entityType->getClass(), RevisionLogInterface::class)) {
      return self::NO_NUM_VAL;
    }

    $count = 0;
    $compareWith = NULL;

    $revisions = $this->getRevisionEntities($entityTypeId, $bundle);
    $entityType = $this->getContentEntityTypeDefinition($entityTypeId);
    $periodTimestamp = $period ? strtotime($period) : NULL;

    foreach ($revisions as $rid => $revisionEntity) {
      if (!$revisionEntity->hasField($fieldName)) {
        continue;
      }

      // Some entity types(e.g: paragraphs) are not defining "revision_created" metadata.
      $revisionColumn = $entityType->getRevisionMetadataKey('revision_created') ?: 'created';
      $creationTimestamp = $revisionEntity->get($revisionColumn)->value;
      if ($period && $creationTimestamp < $periodTimestamp) {
        continue;
      }

      $fieldVal = $revisionEntity->get($fieldName)->getValue();
      if (!$compareWith) {
        $compareWith = $fieldVal;
      }
      else {
        $isEqual = json_encode($compareWith) === json_encode($fieldVal);
        $compareWith = $fieldVal;
        if (!$isEqual) {
          $count++;
        }
      }
    }

    // If one count detected, this is the default one so show it as 0.
    return $count >= 1 ? $count : 0;
  }

  private function getRevisionEntities(string $entityTypeId, $bundle = NULL) {
    static $revisions = [];
    $cid = "{$entityTypeId}__{$bundle}";

    if (!isset($revisions[$cid])) {
      $entityType = $this->getContentEntityTypeDefinition($entityTypeId);
      $revisionKey = $entityType->getKey('revision');
      $entityStorage = $this->entityTypeManager->getStorage($entityTypeId);
      $query = $entityStorage->getQuery();
      if ($bundle && $entityType->hasKey('bundle')) {
        $query->condition($entityType->getKey('bundle'), $bundle);
      }

      $query->allRevisions()
        ->accessCheck(FALSE)
        ->sort($revisionKey, 'ASC');
      $revisionIds = $query->execute();
      $revisions[$cid] = $entityStorage->loadMultipleRevisions(array_keys($revisionIds));
    }

    return $revisions[$cid];
  }

  private function getEntities(string $entityTypeId, $bundle = NULL): array {

    static $entities = [];
    $cid = "{$entityTypeId}__{$bundle}";

    if (!isset($entities[$cid])) {
      $entityType = $this->getContentEntityTypeDefinition($entityTypeId);
      $entityStorage = $this->entityTypeManager->getStorage($entityTypeId);
      $query = $entityStorage->getQuery();
      if ($bundle && $entityType->hasKey('bundle')) {
        $query->condition($entityType->getKey('bundle'), $bundle);
      }
      $query->accessCheck(FALSE);

      $entityIds = $query->execute();
      $entities[$cid] = $entityStorage->loadMultiple($entityIds);
    }

    return $entities[$cid];
  }

  public function getFilterFormats(): array {
    return $this->entityTypeManager->getStorage('filter_format')->loadMultiple();
  }

  public function getFieldsDataOfSupportingFilterFormat(): array {
    return [
      'text' => $this->entityFieldManager->getFieldMapByFieldType('text'),
      'text_long' => $this->entityFieldManager->getFieldMapByFieldType('text_long'),
      'text_with_summary' => $this->entityFieldManager->getFieldMapByFieldType('text_with_summary'),
    ];
  }

}
